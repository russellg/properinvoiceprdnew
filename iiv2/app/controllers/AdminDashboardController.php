<?php

use Carbon\Carbon;

class AdminDashboardController extends BaseController {
 
 
	function __construct()
    {
		 
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		$yearRangeStart = date('Y', time());
		$yearRangeEnd = date('Y', time());
		$monthRangeStart = date('n', time());
		$monthRangeEnd = date('n', time());
		$monthStart = 1;
		
		// Subscriptions
		$total_tenants = Tenant::where('verified', '>', '0')->count();
		$total_starter = Tenant::where('verified', '>', '0')->where('account_plan_id', '=', '1')->count();
		$total_premium = Tenant::where('verified', '>', '0')->where('account_plan_id', '=', '2')->count();
		$total_super_premium = Tenant::where('verified', '>', '0')->where('account_plan_id', '=', '3')->count();
		
		// This month's subscription
		$total_tenants_this_month = Tenant::where('verified', '>', '0')->count();		
		$total_starter_this_month = Tenant::where('verified', '>', '0')->where('account_plan_id', '=', '1')->whereBetween('created_at', array(
		    Carbon::createFromDate($yearRangeStart, $monthRangeStart)->startOfMonth(),
		    Carbon::createFromDate($yearRangeEnd, $monthRangeEnd)->endOfMonth()
		))->count();	
			
		$total_premium_this_month = Tenant::where('verified', '>', '0')->where('account_plan_id', '=', '2')->whereBetween('created_at', array(
		    Carbon::createFromDate($yearRangeStart, $monthRangeStart)->startOfMonth(),
		    Carbon::createFromDate($yearRangeEnd, $monthRangeEnd)->endOfMonth()
		))->count();
				
		$total_super_premium_this_month = Tenant::where('verified', '>', '0')->where('account_plan_id', '=', '3')->whereBetween('created_at', array(
		    Carbon::createFromDate($yearRangeStart, $monthRangeStart)->startOfMonth(),
		    Carbon::createFromDate($yearRangeEnd, $monthRangeEnd)->endOfMonth()
		))->count();
				
	  
		// Sales
		$total_sales = PaymentsHistory::all()->sum('amount');
		  
		$total_sales_this_week = PaymentsHistory::whereBetween('created_at', array(
		   Carbon::createFromDate($yearRangeStart, $monthRangeStart)->startOfWeek(),
		   Carbon::createFromDate($yearRangeEnd, $monthRangeEnd)->endOfWeek()
		))->sum('amount');
		
		$total_sales_this_month = PaymentsHistory::whereBetween('created_at', array(
		    Carbon::createFromDate($yearRangeStart, $monthRangeStart)->startOfMonth(),
		    Carbon::createFromDate($yearRangeEnd, $monthRangeEnd)->endOfMonth()
		))->sum('amount');
		
		
		$total_sales_this_year = PaymentsHistory::whereBetween('created_at', array(
		    Carbon::createFromDate($yearRangeStart, $monthStart)->startOfMonth(),
		    Carbon::createFromDate($yearRangeEnd, $monthRangeEnd)->endOfMonth()
		))->sum('amount');
		
		 	 
        return View::make('admindashboard.index')		 
		->with('title', 'Admin Dashboard')	
		->with(compact('total_tenants'))
		->with(compact('total_starter'))
		->with(compact('total_premium'))
		->with(compact('total_super_premium'))
	 
		->with(compact('total_tenants_this_month'))
		->with(compact('total_starter_this_month'))
		->with(compact('total_premium_this_month'))
		->with(compact('total_super_premium_this_month'))
		
		->with(compact('total_sales'))
		->with(compact('total_sales_this_week'))
		->with(compact('total_sales_this_month'))
		->with(compact('total_sales_this_year'))	 
		->with('script', 'dashboard');
	}

	
	public function report()
	{
		 return View::make('dashboard.report')->with('title', 'Report');
	}

	 
	public function search()
	{
		return View::make('dashboard.search')->with('title', 'Search Results');
	}

}