@extends('layouts.login')
 
	@section('content')
	 
	<div id="login_form">
 
	<a href="http://www.properinvoice.com"><img src="{{ URL::asset('proper_invoice_logo.png') }}" alt="ProperInvoice" style="max-width:300px;"></a>
	<p style="text-align:center; !important">Life is short. Dont waste time Invoicing.</p> <br>
	
	<h1>Log in</h1>
	 
		{{ Form::open(array('route' => 'login')) }}
		
		 <!-- check for login errors flash var -->
		 
		@if (Session::get('flash_message'))
			<div class="flash success">{{ Session::get('flash_message') }}</div>
		@endif
		
		@if (Session::get('failed_flash_message'))
			<div class="flash error">{{ Session::get('failed_flash_message') }}</div>
		@endif
	
		
		 <!-- username field -->
		<p>{{ Form::label('email', 'Email') }}</p>
		<p>{{ Form::text('email') }}</p>
		<!-- password field -->
		<p>{{ Form::label('password', 'Password') }}</p>
		<p>{{ Form::password('password') }}</p>
		<!-- submit button -->
		<p>{{ Form::submit('Login', array('class' => 'btn')) }}</p>

	 {{ Form::close() }}
	 
	  
	 {{ link_to_route('passwordresets', 'Forgot your password?', array(), array('class' => 'link')) }} &nbsp;&nbsp;&nbsp; 
	 <p>Don't have an account yet? <a class="link" href="{{ URL::to('signup') }}">Signup</a><p/> 
	 <!--
	 <a class="link" href="http://www.properinvoice.com">www.properinvoice.com</a>
	 -->
	 
	</div>
 
	@stop