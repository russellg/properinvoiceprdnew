@extends('layouts.default')

	@section('content')
	 
	 <h1><a class="do_previous" href="{{ URL::to('dashboard') }}">&nbsp;<i class="fa fa-home">&nbsp;</i></a>&raquo; Export Data</h1>
 		<h1 style="text-align:center;"> Download your Company Data </h1></br>
 		   {{ Form::open(array('url' => 'invoices/process_export', 'method' => 'POST')) }}
 		   <h3> 1. Download an extract of All Invoices </h3></br>
	           <input type="submit" id="invoices_download" class="gen_btn" name="invoice_download" value="Download Invoices CSV" />  
	       {{ Form::close() }}
	       
	       {{ Form::open(array('url' => 'expenses/process_export', 'method' => 'POST')) }}
	       <h3> 2. Download a list of all your Expenses </h3></br>
	           <input type="submit" id="expenses_download" class="gen_btn" name="expense_download" value="Download Expenses CSV" />  
	       {{ Form::close() }}
	       
 			
	       {{ Form::open(array('url' => 'products/process_export', 'method' => 'POST')) }}
	       <h3> 3. Download a list of Products that your have added to ProperInvoice </h3></br>
	           <input type="submit" id="products_download" class="gen_btn" name="product_download" value="Download Products CSV" />  
	       {{ Form::close() }}
	       
	       {{ Form::open(array('url' => 'services/process_export', 'method' => 'POST')) }}
	       <h3> 4. Download a list of Services that your have added to ProperInvoice </h3></br>
	           <input type="submit" id="services_download" class="gen_btn" name="service_download" value="Download Services CSV" />  
	       {{ Form::close() }}
	       
	       {{ Form::open(array('url' => 'clients/process_export', 'method' => 'POST')) }}
	       <h3> 5. Download a list of your Clients </h3></br>
	           <input type="submit" id="clients_download" class="gen_btn" name="client_download" value="Download Clients CSV" />  
	       {{ Form::close() }}
	       
	       {{ Form::open(array('url' => 'merchants/process_export', 'method' => 'POST')) }}
	       <h3> 6. Download a list of your Merchants / Suppliers  </h3></br>
	           <input type="submit" id="merchant_download" class="gen_btn" name="merchant_download" value="Download Merchants CSV" />  
	       {{ Form::close() }}
  
	@stop
	

	 @section('footer')

	<script>
	
		$(function(){
		 
		 	  if($('#appmenu').length > 0){
				    $('.more_all_menu').addClass('selected_group'); 		 
			  		$('.menu_export_data').addClass('selected');		  		
			  		$('.more_all_menu ul').css({'display': 'block'});
			    }
		 
		});
		
	</script>
	
@stop