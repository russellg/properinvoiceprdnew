@extends('layouts.login')

	
	@section('content')
	<div id="login_form">
	<a href="http://www.properinvoice.com"><img src="{{ URL::asset('proper_invoice_logo.png') }}" alt="ProperInvoice" style="max-width:300px;"></a>
	<h1>Reset your password</h1>

		{{ Form::open(array('url' => 'passwordresets', 'method' => 'POST')) }}
 
		 <!-- username field -->
		<p>{{ Form::label('email', 'Email Address') }}</p>
		<p>{{ Form::text('email', null, array('required' => true)) }}</p>
		<!-- password field -->
		 
		<!-- submit button -->
		<p>{{ Form::submit('Reset', array('class' => 'btn')) }}</p>
		
		<!-- check for login errors flash var -->
		
		@if (Session::get('flash_message'))
		<div class="flash success">{{ Session::get('flash_message') }}</div>
		@endif
		
		@if (Session::get('failed_flash_message'))
			<div class="flash error">{{ Session::get('failed_flash_message') }}</div>
		@endif
	 
	 {{ Form::close() }}
	 
	  <br />
	 	<a class="link" href="{{ URL::to('login') }}">Login</a> &nbsp;&nbsp;&nbsp;
	    <a class="link" href="http://www.properinvoice.com">www.properinvoice.com</a>
 
	</div>
 
	@stop